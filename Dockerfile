FROM ubuntu:22.04
FROM rust:latest

# Update default packages
RUN apt-get update

# Get Ubuntu packages
RUN apt-get install -y \
        build-essential \
        git \ 
        gcc

# Update new packages
RUN apt-get update

RUN useradd -m bharat 



USER bharat
WORKDIR /home/bharat

RUN git clone --recurse-submodules https://gitlab.science.ru.nl/bharat/tomas_bsc_thesis.git
WORKDIR /home/bharat/tomas_bsc_thesis/gsuffix

RUN make clean all

WORKDIR /home/bharat/tomas_bsc_thesis/lsharp_app
RUN cargo b -r
RUN cargo install --path .

WORKDIR /home/bharat/tomas_bsc_thesis