use clap::Parser;
use fnv::FnvBuildHasher;
use itertools::Itertools;
use lsharp_ru::{
    definitions::mealy::{shortest_separating_sequence, InputSymbol, Mealy, OutputSymbol},
    learner::{l_sharp::Lsharp, obs_tree::compressed::CompObsTree},
    oracles::{equivalence::EquivalenceOracle, membership::Oracle as OQOracle},
    sul::{Simulator, SinkWrapper, SystemUnderLearning},
    util::parsers::logs::read as read_logs,
    util::writers::overall as MealyWriter,
};
use std::{cell::RefCell, collections::HashMap, fs, hash::BuildHasherDefault, rc::Rc, sync::Arc};

use crate::suffix_tree_oracle::SuffixOracle;

mod cli;
mod suffix_tree_oracle;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
    ensure_dir_exists("./hypothesis")?;

    let args = cli::Cli::parse();

    let file_name = args.model();
    let log_path = args.traces();
    let gsuffix_path = args.gsuffix();
    let extra_states = *args.extra_states();
    let suffix_len = *args.len_suffix();
    let seed = 5;
    let (fsm, input_map, output_map) =
        prep_fsm_from_file(file_name.as_str()).expect("Error reading from DOT file");
    let sink_output_asml = *output_map
        .get("error")
        .unwrap_or(&OutputSymbol::new(u16::MAX));
    let fsm_logs = log_path
        .as_ref()
        .and_then(|log_p| read_logs(log_p, &input_map, &output_map).ok());
    let mealy_machine = Arc::new(fsm.clone());
    let trial_wo_sink = Simulator::new(&mealy_machine, &input_map, &output_map);

    let mut trial = SinkWrapper::new(trial_wo_sink, sink_output_asml);
    let comp_tree = CompObsTree::<DefaultFxHasher>::new(trial.input_map().len());

    let oq_oracle = Rc::new(RefCell::new(OQOracle::new(
        &mut trial,
        lsharp_ru::learner::l_sharp::Rule2::Ads,
        lsharp_ru::learner::l_sharp::Rule3::Ads,
        sink_output_asml,
        seed,
        comp_tree,
    )));
    let logs = fsm_logs.map(|logs| {
        logs.into_iter()
            .map(|(x, y)| (x.to_vec(), y.to_vec()))
            .collect_vec()
    });

    // The suffix oracle goes here.
    let mut eq_oracle = SuffixOracle::new(
        Rc::clone(&oq_oracle),
        gsuffix_path.to_string(),
        extra_states,
        suffix_len,
        log_path.as_ref().expect("Traces not provided."),
        &input_map,
    );

    // And Learner.
    let mut learner = Lsharp::new(Rc::clone(&oq_oracle), input_map.len(), false);

    let mut idx = 1;
    let mut learn_inputs: usize = 0;
    let mut learn_resets: usize = 0;
    let mut test_inputs: usize = 0;
    let mut test_resets: usize = 0;
    let mut success = false;
    // Set this to whatever you like.

    learner.init_obs_tree(logs);

    loop {
        println!("LOOP {idx}");
        let hypothesis = learner.build_hypothesis();
        let (l_inputs, l_resets) = learner.get_counts();
        learn_inputs += l_inputs;
        learn_resets += l_resets;
        println!("Learning queries/symbols: {l_resets}/{l_inputs}",);

        let writer_config = MealyWriter::WriteConfigBuilder::default()
            .file_name(Some(
                format!("./hypothesis/hypothesis_{idx}.dot").to_string(),
            ))
            .format(MealyWriter::MealyEncoding::Dot)
            .build()
            .expect("Could not write Hypothesis file.");
        {
            let (input_vec, output_vec) = RefCell::borrow(&oq_oracle).pass_maps();
            let rev_input_map = input_vec.into_iter().map(flip).collect();
            let rev_output_map = output_vec.into_iter().map(flip).collect();
            MealyWriter::write_machine::<FnvBuildHasher, _>(
                &writer_config,
                &hypothesis,
                &rev_input_map,
                &rev_output_map,
            )
            .expect("Safe");
        }

        let ideal_ce = shortest_separating_sequence(&fsm.clone(), &hypothesis, None, None);
        if ideal_ce.is_none() {
            success = true;
            break;
        }
        let ce = eq_oracle.find_counterexample(&hypothesis);
        let (t_inputs, t_resets) = eq_oracle.get_counts();
        test_inputs += t_inputs;
        test_resets += t_resets;
        println!("Testing queries/symbols: {t_resets}/{t_inputs}");
        if ce.is_some() {
            println!("CE found, refining observation tree!");
            learner.process_cex(ce, &hypothesis);
        } else {
            println!("No CE found, learning finished.");
            break;
        }
        idx += 1
    }
    println!("Learned : {success}");
    println!("MQ [Resets] : {learn_resets}");
    println!("MQ [Symbols] : {learn_inputs}");
    println!("EQ [Resets] : {test_resets}");
    println!("EQ [Symbols] : {test_inputs}");
    println!("Rounds : {idx}");
    Ok(())
}

fn ensure_dir_exists(dir_name: &str) -> std::io::Result<()> {
    // Do not remove any of these lines
    // Remove the dir (returns an error if it doesn't exist)
    // and ignore the error-case.
    let _ = fs::remove_dir_all(dir_name);
    // Create an empty dir.
    fs::create_dir_all(dir_name)
}

fn flip<A, B>((x, y): (A, B)) -> (B, A) {
    (y, x)
}
type ParseResultFSM = Result<
    (
        Mealy,
        HashMap<String, InputSymbol>,
        HashMap<String, OutputSymbol>,
    ),
    Box<dyn std::error::Error>,
>;
fn prep_fsm_from_file(file_name: &str) -> ParseResultFSM {
    ensure_dir_exists("./hypothesis")?;
    log::info!("Read file from {:?}", file_name);
    Ok(lsharp_ru::util::parsers::machine::read_mealy_from_file(
        file_name,
    ))
}
