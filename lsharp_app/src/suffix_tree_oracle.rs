use itertools::Itertools;
use lsharp_ru::definitions::characterisation::Characterisation;
use lsharp_ru::oracles::equivalence::incomplete::hads::HADSMethod;
use lsharp_ru::util::sequences::FixedInfixGenerator;
use lsharp_ru::util::{access_seqs, toolbox};

use std::collections::HashMap;
use std::io::{BufRead, BufReader, Write};
use std::process::{Command, Stdio};
use std::{cell::RefCell, rc::Rc, thread, time::Duration};

use lsharp_ru::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use lsharp_ru::definitions::FiniteStateMachine;
use lsharp_ru::learner::obs_tree::ObservationTree;
use lsharp_ru::oracles::equivalence::{CounterExample, EquivalenceOracle};
use lsharp_ru::oracles::membership::Oracle as OQOracle;
/// Initial implementation of an oracle which uses a Suffix-Tree.
pub struct SuffixOracle<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    location: String,
    extra_states: usize,
    suffix_len: usize,
    traces_file_path: String,
    input_map: &'a HashMap<String, InputSymbol>,
}

impl<'a, T> EquivalenceOracle<'a, T> for SuffixOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        {
            let sleepy = Duration::from_millis(100);
            thread::sleep(sleepy);
        }
        self.find_counterexample_with_lookahead(hypothesis, self.extra_states)
    }
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        let char_map = HADSMethod::characterisation_map(hypothesis);

        let access_map = access_seqs::access_sequences(
            hypothesis,
            access_seqs::InputSelection::Normal,
            access_seqs::SearchStrategy::Bfs,
        );
        let fig = FixedInfixGenerator::new(
            hypothesis.input_alphabet(),
            lookahead,
            lsharp_ru::util::sequences::Order::ASC,
        );

        let log_suffixes = self.suffixes_of_len(self.suffix_len);
        let mut log_suffixes = log_suffixes.into_iter().map(|x| x.seq).collect_vec();
        if log_suffixes.is_empty() {
            log_suffixes.push(vec![]);
        }
        let mut test_suite = Vec::new();
        for infix in fig.generate() {
            for acc in access_map.values() {
                for sub_seq in &log_suffixes {
                    // Get current state.
                    let s = {
                        let acc_s = hypothesis.trace(acc).0;
                        let i_s = hypothesis.trace_from(acc_s, &infix).0;
                        hypothesis.trace_from(i_s, sub_seq).0
                    };
                    for char_seq in char_map.get(&s).expect("Safe") {
                        let test = toolbox::concat_slices(&[acc, &infix, sub_seq, char_seq]);
                        test_suite.push(test);
                    }
                }
            }
        }

        for input_vec in test_suite {
            let hyp_output = hypothesis.trace(&input_vec).1.to_vec();
            let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(&input_vec);
            if sut_output.len() < hyp_output.len() {
                let hyp_out_lim = &sut_output[..sut_output.len()];
                if hyp_out_lim != sut_output {
                    let ce_input = input_vec[..sut_output.len()].to_vec();
                    let ce_output = sut_output;
                    let ret = Some((ce_input, ce_output));
                    return ret;
                }
            } else if hyp_output != sut_output {
                let ce_input = input_vec;
                let ce_output = sut_output;
                let ret = Some((ce_input, ce_output));
                return ret;
            }
        }
        None
    }
}

impl<'a, T> SuffixOracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync,
{
    pub fn new(
        oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
        location: String,
        extra_states: usize,
        suffix_len: usize,
        traces_file_path: &str,
        input_map: &'a HashMap<String, InputSymbol>,
    ) -> Self {
        Self {
            oq_oracle,
            location,
            extra_states,
            suffix_len,
            traces_file_path: traces_file_path.to_owned(),
            input_map,
        }
    }

    /// Ask for subsequences of length `len`.
    fn suffixes_of_len(&self, len: usize) -> Vec<SubSequence> {
        let mut child = Command::new(&self.location)
            .arg(&self.traces_file_path)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .expect("Could not initialise gsuffix program.");
        let in_stream = child.stdin.as_mut().expect("Safe");
        let cmd = format!("n {len}\n");
        in_stream.write_all(cmd.as_bytes()).expect("Safe");
        in_stream.flush().expect("Could not write to gsuffix!");
        let mut suffixes = Vec::new();
        {
            let child_out = child
                .stdout
                .as_mut()
                .expect("Did not get any sequences from gsuffix?");

            let mut child_reader = BufReader::new(child_out);
            loop {
                let mut seq = String::new();
                child_reader
                    .read_line(&mut seq)
                    .expect("Could not read line from gsuffix.");
                if seq.trim() == "DONE" {
                    break;
                }
                let suffix =
                    SubSequence::parse_seq(&seq, self.input_map).expect("Suffix malformed!");
                suffixes.push(suffix);
            }
        }
        suffixes
    }
}

#[derive(Debug)]
struct SubSequence {
    #[allow(dead_code)]
    freq: usize,
    seq: Vec<InputSymbol>,
}

impl SubSequence {
    fn parse_seq(
        seq: &str,
        input_map: &HashMap<String, InputSymbol>,
    ) -> Result<Self, ParseSubSeqError> {
        let (cnt, seq) = seq.split_once(' ').ok_or(ParseSubSeqError)?;
        let freq = cnt.trim().parse::<usize>().map_err(|_| ParseSubSeqError)?;
        let seq = seq
            .trim()
            .split(' ')
            .map(|s| {
                input_map
                    .get(s)
                    .expect("Input symbol not present in input_map.")
            })
            .copied()
            .collect();
        let ret = SubSequence { freq, seq };
        Ok(ret)
    }
}

#[derive(Debug, PartialEq, Eq)]
struct ParseSubSeqError;
