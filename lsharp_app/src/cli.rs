use clap::Parser;
#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(short = 'm', long)]
    model: String,
    #[arg(long)]
    traces: Option<String>,
    #[arg(long)]
    /// Full path to the gsuffix executable.
    gsuffix: String,
    /// Number of extra states.
    #[arg(short = 'k')]
    extra_states: usize,
    #[arg(short = 's')]
    /// Length of the suffix to look for.
    len_suffix: usize,
}
