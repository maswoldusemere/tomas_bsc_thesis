import sys
from itertools import product
import random as r
import math as m
from functools import reduce

# HELPER FUNCTIONS USED

# FUNCTION TO CHECK IF ITS A PREFIX FUNCTION
def is_prefix(prefix):
    for code in code_words: 
        if code.startswith(prefix):
            return True, code == prefix, code
    return False, False, ''

# ALTER THE INDEX OF THE PRIZE TO 1 (WON).
def alter_prize(prizes, index_of_prize):
    prizes = list(prizes)
    prizes[index_of_prize] = 1
    return tuple(prizes)

# FUNCTION FOR A TRANSITION STRING
tras = lambda cs, ns, i, output = '-' : "\t\"{}\" -> \"{}\" [label=\"{}/{}\"];\n".format(cs, ns, i, output)

# CONCATENATES THE PRIZES WON.
reduce_prize_tuple = lambda prize_tuple : reduce(lambda x, y : str(x) + str(y), prize_tuple)

# FUNCTIONS TO PRINT THE TRACES IN THE FILE
trace = lambda x , y : '\n1 {} {}'.format(x, y)
in_out = lambda inp, out : '{} {} '.format(inp, out)
prize_trace = lambda prize: ' - '.join(prize) + ' - '

# FUNCTION FOR TRANSITION
def transition(state, input):
    s , w = state
    pre, full, _ = is_prefix(w+input)
    if full: # won prize
        ns = alter_prize(s, code_words[w+input])
        return states[(s, w)], states[(ns, '')], input
    elif pre:  # wi prefix
        return states[(s, w)], states[(s, w+input)], input
    elif input == 'p': # view current won prizes and stay on the same state
        return states[(s, w)], states[(s, w)], input, reduce_prize_tuple(s)
    else:  #no prefix / invalid input
        ns = states[init] if w == '' else states[(s, w)]  # if no earlier prefix were recorded then return to init otherwise stay.
        return states[(s,w)], ns, input
        #return states[(s,w)], states[(s,w)], input   return to same state if wrong.

# FUNCTION TO RANDOMLY ADD VALID INPUT OUTPUT TRACES.
def add_junk_trace(current_state):
    no_of_traces = r.randint(0, 10)
    len_traces = 0
    current_trace = ''
    while no_of_traces:
        r_input = r.choice(list(input))
        _, ns, _, *output = transition(current_state, r_input)
        output = '-' if not output else reduce_prize_tuple(output[0])
        current_state = index_states[ns]
        current_trace += in_out(r_input, output)
        len_traces += 2
        no_of_traces-=1
    return current_trace, len_traces, current_state

# FUNCTION TO ADD THE REMAINING PRIZES AFTER LENGTH THRESOLD HAS BEEN MET
def add_remaining_prize(current_prizes):
    rem_prize_trace = ''
    rem_prize_len = 0
    for ind, p in enumerate(current_prizes):
        if p:
            continue
        prize = index_code_words[ind]
        rem_prize_len += 2*len(prize)
        rem_prize_trace+=prize_trace(prize)
        #rem_prize_trace+= ' - '.join(prize) + ' - '
    return rem_prize_trace , rem_prize_len

# ADD THE COMBINATION OF JUNK TRACE AND VALID PRIZE TRACE
def add_junk_and_random_prize_trace(current_state, len_trace):
    current_trace_len = 0
    trace = ''
    while current_trace_len < len_trace:
        j_trace, len_jtrace, current_state = add_junk_trace(current_state)
        _, prefix = current_state
        is_valid_prefix, _, code = is_prefix(prefix)
        chosen_prize = code[len(prefix):] if is_valid_prefix and prefix !='' else r.choice(list(code_words.keys()))
        current_state = (alter_prize(current_state[0], code_words[code]), '')
        trace += j_trace + prize_trace(chosen_prize)
        current_trace_len += len_jtrace + 2*len(chosen_prize)
    return current_state, trace, current_trace_len 

#  ***********BEGIN OF TRACES FILE WRITING******************* 

# READ ARGUMENTS
args = len(sys.argv)
filename = sys.argv[args - 1]
len_logs = int(sys.argv[args - 2])


# CREATE CODE_WORDS, INPUT SYMBOLS, PROPER PREFIXES, INITIAL STATE
code_words = { i : count for count, i in enumerate(sys.argv[1:args-2])}
index_code_words = { i:w for (w, i) in code_words.items()}
input = { y for x in code_words for y in x }
input.add('p')
prefixes = {x[:i] for x in code_words for i in range(len(x))}
prizes_size = len(code_words)
init = ((0,)*prizes_size, '')


# CREATE STATES
states = [(x, y)  for x in product([0,1], repeat=prizes_size) for y in prefixes]
states = { x : count for count, x in enumerate(states)}
index_states = { y : x for (x, y) in states.items()}

# CREATE DOT FILE 
dot_file = open(filename + '.dot', 'x')
dot_file.write('digraph g {\n\n')
dot_file.write("\t__shape0 [label=\"\" shape=\"none\"]\n\n")


# WRITE STATES TO DOT FILE
for key in states:
    dot_file.write("\t\"{0}\" [shape=\"circle\" label=\"{0}\"];\n".format(states[key]))


# WRITE TRANSITIONS TO DOT FILE
for s in states:
    for i in input:
        trans = tras(*transition(s, i))
        dot_file.write(trans)


# CLOSE DOT FILE
dot_file.write("\n\t__shape0 -> \"{}\";\n".format(states[init]))
dot_file.write('}')
dot_file.close()

#  ***********BEGIN OF TRACES FILE WRITING******************* 

# make a new file
traces_file = open(filename+'.traces', 'x')

# write number of the traces and the number of symbols (input & output)
len_inp_out = len(input) + 2**prizes_size + 1  # +1 for the dash
traces_file.write("{} {}".format(len_logs, len_inp_out))

# DEFINE THE MIN AND MAX TRACE LENGTH FOR RANDOM GENERATOR
min_len_trace = 2*m.factorial(prizes_size)
max_len_trace = 10*min_len_trace


# WRITE TRACES TO FILE
while len_logs:
    len_trace = r.randint(min_len_trace, max_len_trace)
    current_state = init
    current_state, current_trace, current_len = add_junk_and_random_prize_trace(current_state, len_trace)
    rem_trace, len_rem_trace = add_remaining_prize(current_state[0])
    current_trace+= rem_trace
    current_len+= len_rem_trace
    traces_file.write(trace(current_len, current_trace))
    len_logs-=1

# close the traces file
traces_file.close()