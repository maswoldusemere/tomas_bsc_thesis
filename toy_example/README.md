# Running the model and traces generator

To generate a model and a traces files for a given codewords with `n` traces we can generate model by running the below command as follows:

``` Bash
python3 generator.py <codewords> <n> <chosen filename>
```
For example, codewords are 19, 62, 22 and n = 100

``` Bash
python3 generator.py 19 62 22 100 g
```
