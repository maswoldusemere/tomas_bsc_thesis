use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::fmt::Debug;
use std::rc::Rc;

use bimap::BiBTreeMap;
use fnv::FnvHashSet;
use itertools::Itertools;
use rayon::prelude::{IntoParallelRefIterator, IntoParallelRefMutIterator, ParallelIterator};
use serde::{Deserialize, Serialize};

// use crate::ads::lee_yannakakis::adaptive_distinguishing_seq::AdsTree as LyAds;
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::oracles::{equivalence::CounterExample, membership::Oracle as OutputOracle};
use crate::util::toolbox::{self, Seq};

use super::apartness::{acc_states_are_apart, compute_witness, tree_and_hyp_compute_witness};
use super::obs_tree::ObservationTree;

/// Struct for the learner.
pub struct Lsharp<'a, Tree> {
    /// OutputQuery Oracle, needed by the Learner.
    oq_oracle: Rc<RefCell<OutputOracle<'a, Tree>>>,
    /// Size of the input alphabet.
    input_size: usize,
    /// List of basis states.
    /// Each state is represented by its access seq.
    basis: Vec<Vec<InputSymbol>>,
    /// Frontier state &mapsto; set of basis candidate states.
    frontier_to_basis_map: BTreeMap<Vec<InputSymbol>, Vec<Vec<InputSymbol>>>,
    /// Use the Lee-Yannakakis optimisation.
    use_ly_ads: bool,
    /// Num of `consistency` CEs generated.
    consistency_ce_count: usize,
    /// Basis state -> Hyp state map.
    basis_map: BiBTreeMap<Vec<InputSymbol>, State>,

    /* For Debugging */
    frontier_transitions_set: FnvHashSet<Vec<InputSymbol>>,
    round: usize,
}

impl<'a, Tree> Lsharp<'a, Tree>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    /// Constructor.
    ///
    /// We require an oracle for asking output queries, (wrapped in an `Rc<RefCell<>>`),
    /// the input alphabet of the FSM to be learned, and a flag for enabling the use of
    /// the Lee-Yannakakis optimisation.
    pub fn new(
        oracle: Rc<RefCell<OutputOracle<'a, Tree>>>,
        input_size: usize,
        use_ly_ads: bool,
    ) -> Self {
        Self {
            oq_oracle: oracle,
            input_size,
            basis: vec![vec![]],
            frontier_to_basis_map: BTreeMap::default(),
            use_ly_ads,
            consistency_ce_count: 0,
            basis_map: BiBTreeMap::default(),
            frontier_transitions_set: HashSet::default(),
            round: 1,
        }
    }

    /// Process the given counterexample in order to find the new state.
    ///
    /// We require the hypothesis in order to find the prefix of input sequence
    /// to the point that the hypothesis and observation tree agree
    /// (i.e., have the same outputs).
    pub fn process_cex(&mut self, counter_example: CounterExample, hypothesis: &Mealy) {
        self.round += 1;
        let (ce_input, ce_output) = counter_example.expect("Safe");
        RefCell::borrow_mut(&self.oq_oracle).add_observation(&ce_input, &ce_output);
        let (_, hyp_output) = hypothesis.trace(&ce_input);
        let prefix_idx = get_ce_prefix_idx(&ce_output, &hyp_output);
        self.process_bin_search(
            &ce_input[..prefix_idx],
            &ce_output[..prefix_idx],
            hypothesis,
        );
    }

    #[allow(clippy::many_single_char_names)]
    fn process_bin_search(
        &mut self,
        ce_input: &[InputSymbol],
        ce_output: &[OutputSymbol],
        hypothesis: &Mealy,
    ) {
        let r: Tree::S = RefCell::borrow(&self.oq_oracle)
            .borrow_tree()
            .get_succ(Tree::S::default(), ce_input)
            .expect("Safe");
        self.update_frontier_and_basis();
        if self.frontier_to_basis_map.contains_key(ce_input)
            || self.basis.contains(&ce_input.to_vec())
        {
            return;
        }
        // We now need to find out which state of the tree is `q` in the hypthesis.
        let q: State = hypothesis.trace(ce_input).0;
        let acc_q_t = self.basis_map.get_by_right(&q).expect("Safe");

        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let q_t = o_tree.get_succ(Tree::S::default(), acc_q_t).expect("Safe");
        // Get the prefix of the CE input that is leading to a frontier state.
        let binding = Seq::from(ce_input.to_vec());
        // We used idx inputs to get to the frontier.
        let x = binding
            .prefix_closure()
            .find(|seq| self.frontier_to_basis_map.contains_key(*seq))
            .expect("Safe")
            .len();
        let y = ce_input.len();
        #[allow(clippy::cast_possible_truncation)]
        #[allow(clippy::cast_precision_loss)]
        #[allow(clippy::cast_sign_loss)]
        let h = ((x as f32 + y as f32) / 2_f32).floor() as usize;

        let sigma_1 = &ce_input[..h];
        let sigma_2 = &ce_input[h..];
        let q_p = hypothesis.trace(sigma_1).0;
        let acc_q_p_t = self.basis_map.get_by_right(&q_p).expect("Safe");
        // NOTE:
        // Be **very** careful here, the order of things matter due to reference
        // invalidation with the CompObsTree: the state r_p, especially, gets
        // invalidated sometimes due to the output query we make.
        // let q_p_t = o_tree.get_succ(S::default(), acc_q_p_t).expect("Safe");
        // let r_p = o_tree.get_succ(S::default(), sigma_1).unwrap();
        // let access_q_p = o_tree.get_access_seq(q_p);

        let eta = compute_witness(o_tree, r, q_t)
            .expect("Could not find witness when it should exist, during CE processing!");
        let output_query = toolbox::concat_slices(&[acc_q_p_t, sigma_2, &eta]);
        drop(temp);
        let sul_response = RefCell::borrow_mut(&self.oq_oracle).output_query(&output_query);
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let q_p_t = o_tree
            .get_succ(Tree::S::default(), acc_q_p_t)
            .expect("Safe");
        let r_p = o_tree.get_succ(Tree::S::default(), sigma_1).unwrap();
        let wit = compute_witness(o_tree, q_p_t, r_p);
        drop(temp);
        if wit.is_some() {
            self.process_bin_search(sigma_1, &ce_output[..sigma_1.len()], hypothesis);
        } else {
            let new_inputs = toolbox::concat_slices(&[acc_q_p_t, sigma_2]);
            self.process_bin_search(&new_inputs, &sul_response[..new_inputs.len()], hypothesis);
        }
    }

    /**
    We loop through the following three rules until the observation tree is adequate:
    1. Explore the frontier (implemented [here](crate::oracles::membership::Oracle::explore_frontier)).
    2. Identify the frontier (implemented [here](crate::oracles::membership::Oracle::identify_frontier)).
    3. Promote a frontier state if possible (internal method of this struct).
    */
    fn make_obs_tree_adequate(&mut self) {
        log::info!(
            "Size of tree after EQ: {}",
            RefCell::borrow(&self.oq_oracle).borrow_tree().size()
        );
        loop {
            log::debug!("Applying rule 2");
            let new_frontier = RefCell::borrow_mut(&self.oq_oracle).explore_frontier(&self.basis);
            log::debug!("Found new frontier states : {:?}", new_frontier);
            self.frontier_to_basis_map.extend(new_frontier);

            // Identify the frontier states
            log::debug!("Applying rule 3");
            for (fs, cands) in &mut self.frontier_to_basis_map {
                if cands.len() <= 1 {
                    continue;
                }
                RefCell::borrow_mut(&self.oq_oracle).identify_frontier(fs, cands);
            }

            // Promote the frontier states if they're isolated.
            self.promote_frontier_state();

            if self.tree_is_adequate() {
                break;
            }
        }
    }

    fn promote_frontier_state(&mut self) {
        let new_bs = self
            .frontier_to_basis_map
            .iter()
            .find(|(_, bs_not_sep)| bs_not_sep.is_empty())
            .map(|(fs, _)| fs);
        let Some(bs) = new_bs else{return;};
        let bs = bs.clone();
        log::debug!("Applying rule 4");
        log::debug!("Promoting {:?}", bs);
        self.basis.push(bs.clone());
        self.frontier_to_basis_map.remove(&bs);
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        // The new basis state is a candidate for the frontier
        // states it is not apart from.
        self.frontier_to_basis_map
            .par_iter_mut()
            .filter(|(fs, _cands)| !acc_states_are_apart(o_tree, fs, &bs))
            .for_each(|(_fs, cands)| {
                cands.push(bs.clone());
            });
    }

    /// If each frontier state is identified, a tree is adequate.
    ///
    /// *We assume that all basis states have been explored*.
    fn tree_is_adequate(&mut self) -> bool {
        self.check_frontier_consistency();
        log::debug!("Basis set: {:?}", self.basis);
        log::debug!("Frontier->Basis mapping: {:?}", self.frontier_to_basis_map);
        let is_not_identified = |c: &Vec<_>| c.len() != 1;
        // If any frontier state is not identified.
        if self.frontier_to_basis_map.values().any(is_not_identified) {
            return false;
        }
        let input_alphabet = toolbox::inputs_iterator(self.input_size).collect_vec();
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let mut basis_ip_pairs = self.basis.iter().cartesian_product(input_alphabet);
        // If any basis state is not extended
        if basis_ip_pairs.any(|(q, i)| {
            let q = o_tree.get_succ(Tree::S::default(), q).expect("Safe");
            o_tree.get_out(q, i).is_none()
        }) {
            return false;
        }
        true
    }

    /// Update the frontier and basis maps, i.e.,
    /// identify/isolate and promote frontier
    /// states until we hit a fixed point.
    fn update_frontier_and_basis(&mut self) {
        {
            let temp = RefCell::borrow(&self.oq_oracle);
            let o_tree = temp.borrow_tree();
            self.frontier_to_basis_map
                .par_iter_mut()
                .for_each(|(fs, cands)| cands.retain(|bs| !acc_states_are_apart(o_tree, fs, bs)));
        }
        self.promote_frontier_state();
        self.check_frontier_consistency();
        {
            let temp = RefCell::borrow(&self.oq_oracle);
            let o_tree = temp.borrow_tree();
            self.frontier_to_basis_map
                .par_iter_mut()
                .for_each(|(fs, cands)| cands.retain(|bs| !acc_states_are_apart(o_tree, fs, bs)));
        }
    }

    /// Make the observation tree adequate and return the hypothesis.
    pub fn build_hypothesis(&mut self) -> Mealy {
        loop {
            self.make_obs_tree_adequate();
            let hyp = self.construct_hypothesis();

            if let Some(ce) = self.check_consistency(&hyp) {
                self.consistency_ce_count += 1;
                self.process_cex(Some(ce), &hyp);
            } else {
                if self.use_ly_ads {
                    // let mut ly_ads = LyAds::new(&hyp);
                    // let temp = RefCell::borrow(&self.oq_oracle);
                    // let tree = temp.borrow_tree();
                    // let hyp_states: Vec<_> = hyp.states().into_iter().collect();
                    // let ot_ads = OtAds::new(tree, &hyp_states);
                    // drop(temp);
                    // if ly_ads.identification_power() > ot_ads.identification_power() * 10f32 {
                    //     self.run_ly_ads(&mut ly_ads);
                    //     continue;
                    // }
                    unimplemented!("FIXME");
                }
                // let tree_size = RefCell::borrow(&self.oq_oracle).borrow_tree().size();
                return hyp;
            }
        }
    }

    /// Construct a hypothesis.
    ///
    /// Here, we need to do some extra processing: in the learner,
    /// we identify basis and frontier states by their access sequences, but
    /// that doesn't work for the `Mealy` machine, so we first assign identifiers
    /// to each access sequence and use those identifiers to construct the hypothesis.
    fn construct_hypothesis(&mut self) -> Mealy {
        self.basis_map.clear();
        self.frontier_transitions_set.clear();
        let basis_map = {
            let mut ret = bimap::BiBTreeMap::new();
            for b_acc in &self.basis {
                #[allow(clippy::cast_possible_truncation)]
                let s = State::new(ret.len() as u32);
                ret.insert(b_acc.clone(), s);
            }
            ret
        };
        self.basis_map.extend(basis_map.into_iter());
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let basis = self.basis.clone();
        let input_alphabet = toolbox::inputs_iterator(self.input_size).collect_vec();
        let mut hyp_output_alphabet = HashSet::<_, _>::default();
        let mut loopbacks = FnvHashSet::default();
        log::debug!("Basis set: {:?}", self.basis);
        log::debug!("Frontier-Basis sep: {:?}", self.frontier_to_basis_map);
        let mut trans_fn = HashMap::default();
        for (q, i) in Itertools::cartesian_product(basis.iter(), input_alphabet.iter()) {
            let bs = o_tree.get_succ(Tree::S::default(), q).expect("Safe");
            let output: OutputSymbol = o_tree.get_out(bs, *i).expect("Something is wrong");
            hyp_output_alphabet.insert(output);
            let f_acc = toolbox::concat_slices(&[q, &[*i]]);

            let (dest, is_loopback) = self.identify_frontier_or_basis(&f_acc);
            if is_loopback {
                loopbacks.insert(f_acc.clone());
            }

            let hyp_bs = self.basis_map.get_by_left(q).copied().expect("Safe");
            let hyp_dest = self.basis_map.get_by_left(dest).copied().expect("Safe");
            trans_fn.insert((hyp_bs, *i), (hyp_dest, output));
        }
        // let loopbacks: Vec<_> = loopbacks.into_iter().collect();
        // let loopbacks_str = serde_json::to_string(&loopbacks).expect("Safe");
        // {
        //     let file_name = format!("./hypothesis/loopbacks_hypothesis_{}.txt", self.round);
        //     let file_name = std::fs::canonicalize(file_name).expect("Safe");
        //     let buffer = File::create(file_name).expect("Safe");
        //     serde_json::to_writer(buffer, &loopbacks_str).expect("Safe");
        // }
        log::info!("Size of the tree: {}", o_tree.size());
        let fsm = Mealy::new(
            self.basis_map.right_values().copied().collect(),
            State::new(0),
            input_alphabet,
            hyp_output_alphabet,
            trans_fn,
        );
        fsm
    }

    fn identify_frontier_or_basis<'l>(
        &'l self,
        seq: &'l Vec<InputSymbol>,
    ) -> (&Vec<InputSymbol>, bool) {
        if self.basis.contains(seq) {
            (seq, false)
        } else {
            let bs = self
                .frontier_to_basis_map
                .get(seq)
                .expect("Missing a frontier state")
                .iter()
                .exactly_one()
                .expect("Multiple basis candidates for a single frontier state.");
            (bs, true)
        }
    }

    //    #[allow(unused)]
    //    #[allow(clippy::unused_self)]
    //    fn run_ly_ads(&mut self, _ly_ads: &mut LyAds) {
    //        todo!()
    //    }

    /// Initialise the observation tree, adding logs if provided.
    pub fn init_obs_tree(&mut self, logs: Option<Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>>) {
        if let Some(logs) = logs {
            let mut oracle_ref = self.oq_oracle.borrow_mut();
            for (input_seq, output_seq) in logs {
                oracle_ref.add_observation(&input_seq, &output_seq);
            }
        }
    }

    /// Check if the frontier is consistent with our data structures.
    /// CE processing may add a new frontier state that we miss.
    fn check_frontier_consistency(&mut self) {
        let basis_set = self.basis.iter();
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let ia = toolbox::inputs_iterator(self.input_size).collect_vec();
        let state_input_iterator = Itertools::cartesian_product(basis_set, ia);
        let new_front = state_input_iterator
            .filter_map(|(bs, i)| {
                let fs_acc = toolbox::concat_slices(&[bs, &[i]]);
                if o_tree.get_succ(Tree::S::default(), &fs_acc).is_some() {
                    Some(fs_acc)
                } else {
                    None
                }
            })
            .filter(|x| !self.basis.contains(x))
            .filter(|x| !self.frontier_to_basis_map.contains_key(x))
            .map(|fs| {
                let cands: Vec<_> = self
                    .basis
                    .par_iter()
                    .filter(|&s| !acc_states_are_apart(o_tree, &fs, s))
                    .cloned()
                    .collect();
                (fs, cands)
            })
            .collect::<Vec<_>>();
        self.frontier_to_basis_map.extend(new_front);
    }

    /// Checks whether the hypothesis and the observation tree agree.
    #[must_use]
    fn check_consistency(&self, hypothesis: &Mealy) -> CounterExample {
        let temp = RefCell::borrow(&self.oq_oracle);
        let o_tree = temp.borrow_tree();
        let wit =
            tree_and_hyp_compute_witness(o_tree, Tree::S::default(), hypothesis, State::new(0))?;
        let os = o_tree.get_observation(None, &wit);
        log::info!(
            "Internal tree CE found @ Qsize: {}.",
            hypothesis.states().len()
        );
        let outputs = os.expect("Witness between tree and hyp did not have an output!");
        Some((wit, outputs))
    }

    #[allow(clippy::unused_self)]
    #[must_use]
    pub fn get_ads_score(&self) -> f32 {
        0.0
    }

    /// Number of inputs and number of resets
    #[must_use]
    pub fn get_counts(&self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }
}

/// At which point do the SUL's output and hypothesis' output disagree?
fn get_ce_prefix_idx(ce_output: &[OutputSymbol], hyp_output: &[OutputSymbol]) -> usize {
    ce_output
        .iter()
        .zip(hyp_output.iter())
        .enumerate()
        .find(|(_, (ce_o, he_o))| *ce_o != *he_o)
        .map(|x| x.0)
        .expect("SUL and HYP do not disagree on CE inputs.")
}

/// Options for application of rule 2 of the L# algorithm.
#[derive(PartialEq, Eq, Debug, Default, Clone, clap::ValueEnum, Serialize, Deserialize)]
pub enum Rule2 {
    /// Identify the new frontier state using an ADS (*default*).
    #[default]
    Ads,
    /// Basic version, do not attempt to identify the new frontier state.
    Nothing,
    /// Identify the new frontier state using a (randomly selected) witness for two basis states.
    SepSeq,
}

/// Options for application of rule 3 of the L# algorithm.
#[derive(PartialEq, Eq, Clone, Default, Debug, clap::ValueEnum, Serialize, Deserialize)]
pub enum Rule3 {
    /// Identify frontier states using an ADS (*default*).
    #[default]
    Ads,
    /// Identify frontier states using a (randomly selected) witness for two basis states.
    SepSeq,
}
