use datasize::DataSize;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{
    shortest_separating_sequence, InputSymbol, Mealy, OutputSymbol, State,
};
use lsharp_ru::definitions::FiniteStateMachine;
use lsharp_ru::learner::l_sharp::{Lsharp, Rule2, Rule3};
use lsharp_ru::learner::obs_tree::{compressed::CompObsTree, normal::MapObsTree, ObservationTree};
use lsharp_ru::oracles::equivalence::chained::ChainedEO;
use lsharp_ru::oracles::equivalence::generic::{CharStyle, EOParams, GenericEO};
use lsharp_ru::oracles::equivalence::incomplete::logged_iads;
use lsharp_ru::oracles::equivalence::soucha::{
    self, ConfigBuilder as SouchaConfigBuilder, Oracle as SouchaOracle,
};
use lsharp_ru::oracles::equivalence::statechum_w::ExtWOracle;
use lsharp_ru::oracles::equivalence::EquivalenceOracle;
use lsharp_ru::oracles::equivalence::InfixStyle;
use lsharp_ru::oracles::membership::Oracle as OQOracle;
use lsharp_ru::sul::Simulator;
use lsharp_ru::sul::{SinkWrapper, SystemUnderLearning};
use lsharp_ru::util::writers::overall as MealyWriter;

use fnv::FnvHashMap;
use lsharp_ru::oracles::equivalence::incomplete::iads::IadsEO;
use rayon::prelude::ParallelIterator;

use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::{BuildHasher, BuildHasherDefault};
use std::rc::Rc;
use std::sync::Arc;

use super::learning_config::{EqOracle, LearnResult};

#[derive(derive_builder::Builder)]
pub struct Options {
    pub rule2_mode: Rule2,
    pub rule3_mode: Rule3,
    pub oracle_choice: EqOracle,
    pub seed: u64,
    pub extra_states: usize,
    pub expected_rnd_length: usize,
    pub use_ly_ads: bool,
    pub infix_style: InfixStyle,
    pub quiet: bool,
    pub comp_tree: bool,
}

type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
type _WrappedOracle<'a> = Rc<RefCell<OQOracle<'a, CompObsTree<DefaultFxHasher>>>>;
type _UnCompressedWrappedOracle<'a> = Rc<RefCell<OQOracle<'a, MapObsTree<DefaultFxHasher>>>>;

/// Main function that learns the FSM.
#[allow(clippy::type_complexity)]
#[allow(clippy::too_many_lines)]
#[must_use]
pub fn learn_fsm<S: BuildHasher + Default>(
    sul: &Mealy,
    input_map: &HashMap<String, InputSymbol, S>,
    output_map: &HashMap<String, OutputSymbol, S>,
    options: &Options,
    logs: Option<Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>>,
) -> LearnResult {
    let mealy_machine = Arc::new(sul.clone());
    log::info!("Qsize : {}", mealy_machine.as_ref().states().len());
    log::info!("Isize : {}", mealy_machine.as_ref().input_alphabet().len());
    let start = chrono::Utc::now();
    let trial_wo_sink = Simulator::new(&mealy_machine, input_map, output_map);

    // if we don't have an ASML model, we'll simply choose an impossible output symbol as undef.
    let sink_output_asml = *output_map
        .get("error")
        .unwrap_or(&OutputSymbol::new(u16::MAX));
    let logs = logs.map(|logs| {
        logs.into_iter()
            .map(|(x, y)| (x.to_vec(), y.to_vec()))
            .collect_vec()
    });
    let mut trial = SinkWrapper::new(trial_wo_sink, sink_output_asml);

    let comp_tree = CompObsTree::<DefaultFxHasher>::new(trial.input_map().len());
    let _naive_tree = MapObsTree::<DefaultFxHasher>::new(trial.input_map().len());

    let oq_oracle = Rc::new(RefCell::new(OQOracle::new(
        &mut trial,
        options.rule2_mode.clone(),
        options.rule3_mode.clone(),
        sink_output_asml,
        options.seed,
        // naive_tree,
        comp_tree,
    )));

    if options.comp_tree {
        log::info!("TreeType : Compressed");
    } else {
        log::info!("TreeType : Naive");
    }
    let rev_input_map: FnvHashMap<_, _> = input_map.iter().map(|(x, y)| (*y, x.clone())).collect();

    let rev_output_map: FnvHashMap<_, _> =
        output_map.iter().map(|(x, y)| (*y, x.clone())).collect();

    let mut learner: Lsharp<_> = Lsharp::new(
        Rc::clone(&oq_oracle),
        mealy_machine.input_alphabet().len(),
        options.use_ly_ads,
    );

    let mut idx = 1;
    let success: bool;
    let mut learn_inputs: usize = 0;
    let mut learn_resets: usize = 0;
    let mut test_inputs: usize = 0;
    let mut test_resets: usize = 0;
    let mut hyp_states;

    let mut eq_oracle: Box<dyn EquivalenceOracle<_>> = equivalence_oracle_gen(
        options,
        Rc::clone(&oq_oracle),
        &logs,
        &rev_input_map,
        &rev_output_map,
    );
    // let mut eq_oracle: Box<dyn EquivalenceOracle<_>> =
    learner.init_obs_tree(logs);
    loop {
        if !options.quiet {
            println!("LOOP {idx}");
        }
        let hypothesis = learner.build_hypothesis();
        let (l_inputs, l_resets) = learner.get_counts();
        learn_inputs += l_inputs;
        learn_resets += l_resets;
        if !options.quiet {
            println!("Learning queries/symbols: {l_resets}/{l_inputs}",);
        }
        log::info!("Learning queries/symbols: {l_resets}/{l_inputs}");
        hyp_states = hypothesis.states().len();
        if !options.quiet {
            println!(
                "Hypothesis size: {} and model size: {}",
                hyp_states,
                sul.states().len()
            );
            print_hypothesis::<S, _>(idx, &oq_oracle, &hypothesis);
        }
        let ideal_ce = shortest_separating_sequence(&sul.clone(), &hypothesis, None, None);
        if ideal_ce.is_none() {
            success = true;
            if !options.quiet {
                println!("Learning finished!");
            }
            break;
        }
        log::info!(
            "Length of shortest separating sequence: {}",
            ideal_ce.as_ref().map_or(0, |x| x.0.len())
        );
        log::info!(
            "Ideal CE: {:?}",
            ideal_ce.as_ref().map_or(&vec![], |x| &x.0)
        );
        if !options.quiet {
            println!("Searching for CEX...");
        }
        let ce = eq_oracle.find_counterexample(&hypothesis);
        let (t_inputs, t_resets) = eq_oracle.get_counts();
        test_inputs += t_inputs;
        test_resets += t_resets;
        if !options.quiet {
            println!("Testing queries/symbols: {t_resets}/{t_inputs}");
        }
        log::info!("Testing queries/symbols: {}/{}", t_resets, t_inputs);

        let ce_len = ce.as_ref().map_or(0, |f| f.0.len());
        log::info!("EQStats:{{'Iter':{idx},'HypSize':{hyp_states},'CESize':{ce_len}}}");
        if ce.is_some() {
            log::info!("CE: {:?}", ce.clone().expect("Safe").0);
            if !options.quiet {
                println!("CE found, refining observation tree!");
            }
            learner.process_cex(ce, &hypothesis);
        } else {
            if !options.quiet {
                println!("No CE found.");
            }
            success = ideal_ce.is_none();
            if !options.quiet {
                println!("Model learned: {success}");
            }
            break;
        }
        idx += 1;
    }
    let temp = RefCell::borrow(&oq_oracle);
    let tree = temp.borrow_tree();

    log::info!("OTreeSize: {}", tree.estimate_heap_size());

    // if !options.comp_tree {
    //     num_non_branching_nodes(tree);
    // }
    log::info!("MQ [Resets] : {learn_resets}");
    log::info!("MQ [Symbols] : {learn_inputs}");
    log::info!("EQ [Resets] : {test_resets}");
    log::info!("EQ [Symbols] : {test_inputs}");
    log::info!("Rounds : {idx}");
    log::info!("Searching for counterexample [ms] : {}", 0);
    let stop = chrono::Utc::now();
    let time_taken = stop - start;
    log::info!("Learning [ms] : {}", time_taken.num_milliseconds());
    let equivalent = |learned| {
        if learned {
            "OK"
        } else {
            "NOK"
        }
    };
    log::info!("Equivalent : {}", equivalent(success));
    let ads_score = learner.get_ads_score();
    LearnResult {
        learn_inputs,
        learn_resets,
        eq_oracle: options.oracle_choice.clone(),
        rounds: idx,
        success,
        test_inputs,
        test_resets,
        num_inputs: rev_input_map.len(),
        num_states: hyp_states,
        ads_score,
    }
}

fn num_non_branching_nodes<Tree>(tree: &Tree)
where
    Tree: ObservationTree<InputSymbol, OutputSymbol, S = State> + Send + Sync,
{
    let inputs = lsharp_ru::util::toolbox::inputs_iterator(tree.input_size()).collect_vec();
    let has_exactly_one_child = |s: State| {
        let num_succs = inputs
            .iter()
            .filter(|i| tree.get_out(s, **i).is_some())
            .count();
        num_succs == 1
    };

    let is_leaf = |s: State| {
        let num_succs = inputs
            .iter()
            .filter(|i| tree.get_out(s, **i).is_some())
            .count();
        num_succs == 0
    };

    let tree_size = tree.size();
    log::info!("OTreeNodes: {}", tree_size);
    let states = rayon::prelude::ParallelIterator::map(
        rayon::prelude::IntoParallelIterator::into_par_iter(0..tree_size),
        |x| State::new(x as u32),
    );
    let num_inner_singletons = states.clone().filter(|s| has_exactly_one_child(*s)).count();
    log::info!("OTreeNonBranchingNodes: {}", num_inner_singletons);
    let num_leaves = states.filter(|s| is_leaf(*s)).count();
    log::info!("OTreeLeaves: {}", num_leaves);
}

/// This function is incredibly ugly, but that's fine, its just a provider of the equivalence
/// oracle.
fn equivalence_oracle_gen<'sul, Tree>(
    options: &'sul Options,
    oq_oracle: Rc<RefCell<OQOracle<'sul, Tree>>>,
    logs: &Option<Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>>,
    rev_input_map: &FnvHashMap<InputSymbol, String>,
    rev_output_map: &FnvHashMap<OutputSymbol, String>,
) -> Box<dyn EquivalenceOracle<'sul, Tree> + 'sul>
where
    Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync + Debug + 'sul,
{
    let gen_eo_params = |char_style| {
        EOParams::new(
            options.extra_states,
            options.expected_rnd_length,
            options.seed,
            char_style,
            options.infix_style,
        )
    };
    log::info!("Infix mode : {}", options.infix_style);
    let logged_hads_oracle = {
        let gen_eo = gen_eo_params(CharStyle::Hads);
        let traces = logs
            .as_deref()
            .unwrap_or_default()
            .iter()
            .map(|x| x.0.clone())
            .collect_vec();
        lsharp_ru::oracles::equivalence::logs_generic::GenericEO::new(
            Rc::clone(&oq_oracle),
            gen_eo,
            traces,
        )
    };
    let int_hads_oracle = {
        let generic_eo = gen_eo_params(CharStyle::Hads);
        GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    };
    let int_w_oracle = {
        let generic_eo = gen_eo_params(CharStyle::W);
        GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    };
    let int_wp_oracle = {
        let generic_eo = gen_eo_params(CharStyle::Wp);
        GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    };
    let int_hsi_oracle = {
        let generic_eo = gen_eo_params(CharStyle::Hsi);
        GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    };
    let int_basic_oracle = {
        let generic_eo = gen_eo_params(CharStyle::SepSeq);
        GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    };
    let soucha_ctt = match options.oracle_choice {
        EqOracle::SouchaH => soucha::Method::H,
        EqOracle::SouchaHSI => soucha::Method::HSI,
        EqOracle::SouchaSPY => soucha::Method::SPY,
        EqOracle::SouchaSPYH => soucha::Method::SPYH,
        _ => soucha::Method::Unused,
    };
    let soucha_config = SouchaConfigBuilder::default()
        .exec_loc("/Users/bharat/research/FSMlib/fsm_lib")
        .lookahead(options.extra_states)
        .method(soucha_ctt)
        .build()
        .expect("Incorrect/Missing params from Soucha oracles.");
    let soucha_oracle = SouchaOracle::new(
        Rc::clone(&oq_oracle),
        soucha_config,
        rev_input_map.clone(),
        rev_output_map.clone(),
    );
    let iads_oracle = IadsEO::new(
        Rc::clone(&oq_oracle),
        options.extra_states,
        gen_eo_params(CharStyle::Hads),
    );
    let ext_w_oracle = ExtWOracle::new(
        Rc::clone(&oq_oracle),
        String::new(),
        options.extra_states,
        rev_input_map.clone(),
        rev_output_map.clone(),
    );
    let logged_iads_oracle = {
        let traces = logs
            .as_deref()
            .unwrap_or_default()
            .iter()
            .map(|x| x.0.clone())
            .collect_vec();
        logged_iads::IadsEO::new(
            Rc::clone(&oq_oracle),
            options.extra_states,
            gen_eo_params(CharStyle::Hads),
            traces,
        )
    };
    let ext_w_oracle_chain = ExtWOracle::new(
        Rc::clone(&oq_oracle),
        "/Users/bharat/software/statechum".to_owned(),
        options.extra_states,
        rev_input_map.clone(),
        rev_output_map.clone(),
    );
    // let int_w_oracle_chain = {
    //     let generic_eo = gen_eo_params(CharStyle::W);
    //     GenericEO::new(Rc::clone(&oq_oracle), generic_eo)
    // };
    let logged_hads_oracle_chain = {
        let gen_eo = gen_eo_params(CharStyle::Hads);
        let traces = logs
            .as_deref()
            .unwrap_or_default()
            .iter()
            .map(|x| x.0.clone())
            .collect_vec();
        lsharp_ru::oracles::equivalence::logs_generic::GenericEO::new(
            Rc::clone(&oq_oracle),
            gen_eo,
            traces,
        )
    };
    let mut chained_eo = ChainedEO::default();
    chained_eo.add_oracle(Box::new(ext_w_oracle_chain));
    chained_eo.add_oracle(Box::new(logged_hads_oracle_chain));

    // let seq_oracle = SequenceOracle::new(Rc::clone(&oq_oracle), options.extra_states, rng.gen());
    match options.oracle_choice {
        EqOracle::HadsInt => Box::new(int_hads_oracle),
        EqOracle::Iads => Box::new(iads_oracle),
        EqOracle::Internal | EqOracle::HadsTree => unreachable!(),
        // EqOracle::SepSeq => Box::new(seq_oracle),
        EqOracle::SouchaH | EqOracle::SouchaHSI | EqOracle::SouchaSPY | EqOracle::SouchaSPYH => {
            Box::new(soucha_oracle)
        }
        EqOracle::LoggedHads => Box::new(logged_hads_oracle),
        EqOracle::LoggedIads => Box::new(logged_iads_oracle),
        EqOracle::W => Box::new(int_w_oracle),
        EqOracle::Wp => Box::new(int_wp_oracle),
        EqOracle::Hsi => Box::new(int_hsi_oracle),
        EqOracle::Separating => Box::new(int_basic_oracle),
        EqOracle::ExtW => Box::new(ext_w_oracle),
        EqOracle::Chained => Box::new(chained_eo),
        _ => unreachable!(),
    }
}

fn flip<A, B>((x, y): (A, B)) -> (B, A) {
    (y, x)
}

fn print_hypothesis<S: BuildHasher + Default, T>(
    idx: usize,
    oq_oracle: &Rc<RefCell<OQOracle<T>>>,
    hypothesis: &Mealy,
) {
    let writer_config = MealyWriter::WriteConfigBuilder::default()
        .file_name(Some(format!("./hypothesis/hypothesis_{idx}.dot")))
        .format(MealyWriter::MealyEncoding::Dot)
        .build()
        .expect("Could not write Hypothesis file.");
    {
        let (input_vec, output_vec) = RefCell::borrow(oq_oracle).pass_maps();
        let rev_input_map = input_vec.into_iter().map(flip).collect();
        let rev_output_map = output_vec.into_iter().map(flip).collect();
        MealyWriter::write_machine::<S, _>(
            &writer_config,
            hypothesis,
            &rev_input_map,
            &rev_output_map,
        )
        .expect("Could not write hypothesis to file.");
    }
}
