use std::{collections::HashMap, default::Default, hash::BuildHasher, io::Write};

use derive_builder::Builder;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};
use crate::definitions::FiniteStateMachine;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MealyEncoding {
    Soucha,
    Dot,
}

impl Default for MealyEncoding {
    fn default() -> Self {
        Self::Dot
    }
}

#[derive(Debug, Default, Builder)]
pub struct WriteConfig {
    file_name: Option<String>,
    format: MealyEncoding,
}

impl WriteConfig {
    fn encoding(&self) -> MealyEncoding {
        self.format
    }
    fn file(&self) -> Option<String> {
        self.file_name.clone()
    }
}

/// # Errors
/// If we're unable to write to a file.
pub fn write_machine<S: BuildHasher + Default, Machine: FiniteStateMachine>(
    config: &WriteConfig,
    fsm: &Machine,
    input_rev_map: &HashMap<InputSymbol, String, S>,
    output_rev_map: &HashMap<OutputSymbol, String, S>,
) -> std::io::Result<Vec<u8>> {
    let byte_mealy = match config.encoding() {
        MealyEncoding::Soucha => super::soucha_fmt::write_to_soucha::<S, Machine>(fsm),
        MealyEncoding::Dot => super::dot_fmt::write_to_dot(fsm, input_rev_map, output_rev_map),
    }?;
    if let Some(file) = config.file() {
        write_to_file(&file, &byte_mealy)?;
    }
    Ok(byte_mealy)
}

/// Accepts a file name, fsm, input, and output conversion maps and writes FSM
/// to the file.
///
/// # Errors
/// If `file` could not be created or written to.
fn write_to_file(file: &str, byte_mealy: &[u8]) -> std::io::Result<()> {
    let mut f = std::fs::File::create(file)?;
    f.write_all(byte_mealy)?;
    Ok(())
}
