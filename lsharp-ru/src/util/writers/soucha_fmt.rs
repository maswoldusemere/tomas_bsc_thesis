use crate::definitions::mealy::{InputSymbol, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

use itertools::Itertools;

use std::{hash::BuildHasher, io::Write};

/// Convert a Mealy Machine to Soucha's textual format.
///
/// # Errors
/// If writing to the internal buffers fail.
pub fn write_to_soucha<S: BuildHasher + Default, Machine: FiniteStateMachine>(
    fsm: &Machine,
) -> std::io::Result<Vec<u8>> {
    let t = 2; // Always a mealy machine.
    let r = 1; // Always reduced.
    let num_states = fsm.states().len(); // Num states
    let num_inputs = fsm.input_alphabet().len(); // Num inputs
    let num_outputs = fsm.output_alphabet().len(); // Num outputs
    let max_state_id = num_states; // Indexing at 0.
    let mut vec = Vec::new();

    // First line: type reduced_or_not
    writeln!(&mut vec, "{t} {r}")?;

    // Second line: num_states num_inputs num_outputs
    writeln!(&mut vec, "{num_states} {num_inputs} {num_outputs}")?;

    // Third line: max_state_id
    writeln!(&mut vec, "{max_state_id}")?;

    let state_mapping = normalise_states(fsm);
    let output_mapping = normalise_outputs(fsm);
    // n lines of output function.
    // Follows the format of
    // state_id (input1's output) (input2's output) ... (inputN's output)
    for state_id in 0..num_states {
        write!(&mut vec, "{state_id}")?;
        for input in 0..num_inputs {
            let output_symbol = fsm
                .step_from(
                    *state_mapping.get(state_id).expect("Safe"),
                    InputSymbol::new(input.try_into().expect("Safe")),
                )
                .1;
            let output_raw = output_mapping.binary_search(&output_symbol).expect("Safe");
            write!(&mut vec, " {output_raw}")?;
        }
        writeln!(&mut vec)?;
    }

    // n lines of transition function.
    // Follows the format of
    // state_id (input1's dest) ... (inputN's dest)
    for state_id in 0..num_states {
        write!(&mut vec, "{state_id}")?;
        for input in 0..num_inputs {
            let dest_state = fsm
                .step_from(
                    *state_mapping.get(state_id).expect("Safe"),
                    InputSymbol::new(input.try_into().expect("Safe")),
                )
                .0;
            let state_raw = state_mapping.binary_search(&dest_state).expect("Safe");
            // let state_raw = dest_state.raw();
            write!(&mut vec, " {state_raw}")?;
        }
        writeln!(&mut vec)?;
    }
    Ok(vec)
}

fn normalise_outputs<M: FiniteStateMachine>(fsm: &M) -> Vec<OutputSymbol> {
    let states = fsm.states();
    let inputs = fsm.input_alphabet();
    let mut ret: Vec<_> = states
        .into_iter()
        .cartesian_product(inputs.iter().copied())
        .map(|(s, i)| fsm.step_from(s, i).1)
        .collect();
    ret.sort();
    ret.into_iter().dedup().collect_vec()
}

fn normalise_states<M: FiniteStateMachine>(fsm: &M) -> Vec<State> {
    let mut ret: Vec<_> = fsm.states().into_iter().collect();
    ret.sort();
    ret
}
