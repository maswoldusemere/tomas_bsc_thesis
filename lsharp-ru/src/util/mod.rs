/*!
Utilities module.

Contains modules for data structures and parsing.
*/

pub mod access_seqs;
pub(crate) mod data_structs;
pub mod parsers;
pub mod sequences;
pub mod toolbox;
pub mod writers;
