use std::collections::{BTreeMap, BTreeSet, HashMap, VecDeque};

use crate::definitions::characterisation::Characterisation;
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::{data_structs::arena_tree::ArenaTree, toolbox};

use itertools::Itertools;
use rustc_hash::{FxHashMap, FxHashSet};

use super::tree::SplittingTree;

pub struct HADSMethod;

fn splits(fsm: &Mealy, s: State, t: State, seq: &[InputSymbol]) -> bool {
    let s_out = fsm.trace_from(s, seq).1;
    let t_out = fsm.trace_from(t, seq).1;
    s_out != t_out
}

impl Characterisation for HADSMethod {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let mut char_set = BTreeMap::default();
        let ads = AdsTree::new(fsm);
        let mut ident_map = ads.local_ident_seqs(0);

        let hsi_seqs = super::hsi::HSIMethod::characterisation_map(fsm);
        for s in fsm.states() {
            let empty_ident = IdentStatus::new(&[], false);
            ident_map.entry(s).or_insert(empty_ident);
        }

        // For each state in ident_map which is not fully identified,
        // append all the hsi seqs in the splitting seq already present
        // in ident_map for that state.
        for (s, ident) in ident_map {
            let ident_set: BTreeSet<Vec<InputSymbol>> = if ident.fully_split() {
                [ident.seq].into()
            } else {
                let mut ret = BTreeSet::default();
                let prefix = ident.seq.clone();
                let curr_s = fsm.trace_from(s, &prefix).0;
                // We must separate from the states that `prefix` does not already separate us from!
                let states = fsm.states().into_iter().filter(|t| *t != s).collect_vec();
                let mut to_split: FxHashSet<_> = states
                    .into_iter()
                    .filter(|t| !splits(fsm, s, *t, &prefix))
                    .map(|t| fsm.trace_from(t, &prefix).0)
                    .collect();
                let mut hsi_local = hsi_seqs
                    .get(&curr_s)
                    .expect("Safe")
                    .clone()
                    .into_iter()
                    .collect_vec();
                hsi_local.sort_unstable_by_key(|b| std::cmp::Reverse(b.len()));
                while !to_split.is_empty() {
                    for suffix in &hsi_local {
                        let splits_local: FxHashSet<_> = to_split
                            .iter()
                            .copied()
                            .filter(|t| splits(fsm, curr_s, *t, suffix))
                            .collect();
                        if splits_local.is_disjoint(&to_split) {
                            continue;
                        }
                        let seq = toolbox::concat_slices(&[&prefix, suffix]);
                        ret.insert(seq);
                        to_split.retain(|e| !splits_local.contains(e));
                    }
                }
                ret
            };
            char_set.insert(s, ident_set);
        }
        char_set
    }
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
struct AdsNode {
    pub initial: Vec<State>,
    current: Vec<State>,
    input: Option<InputSymbol>,
    chidren: FxHashMap<OutputSymbol, usize>,
}

impl AdsNode {
    fn construct(initial: Vec<State>, current: Vec<State>) -> Self {
        Self {
            initial,
            current,
            ..Default::default()
        }
    }

    fn from_block(block: impl Iterator<Item = State>) -> Self {
        let initial: Vec<_> = block.collect();
        let current = initial.clone();
        Self {
            initial,
            current,
            ..Default::default()
        }
    }
}

#[derive(Debug, Default)]
struct AdsTree {
    tree: ArenaTree<AdsNode, ()>,
    roots: Vec<usize>,
    initial_idx: usize,
    curr_idx: usize,
}

impl AdsTree {
    /// Make a new (partial) ADS from the given `fsm`.
    fn new(fsm: &Mealy) -> Self {
        let mut ret = Self::default();
        let states = fsm.states();
        let stree = SplittingTree::new(fsm, &states);
        ret.construct(&stree, fsm, &states);
        ret.initial_idx = 0;
        ret.curr_idx = 0;
        ret
    }

    fn local_ident_seqs(&self, root: usize) -> FxHashMap<State, IdentStatus> {
        // There's only one root node in this case: node 0.
        let mut work_list = VecDeque::from([(vec![], root)]);
        let mut idents = FxHashMap::<State, IdentStatus>::default();
        while let Some((unique_ip, curr_node)) = work_list.pop_front() {
            let c_node = self.tree.ref_at(curr_node);
            if let Some(next_ip) = c_node.input {
                for &next_node in c_node.chidren.values() {
                    let mut new_ip_seq = unique_ip.clone();
                    new_ip_seq.push(next_ip);
                    work_list.push_back((new_ip_seq, next_node));
                }
            } else {
                let fully_split = c_node.initial.len() == 1;
                for &state in &self.tree.ref_at(curr_node).initial {
                    let local_ident = IdentStatus::new(&unique_ip, fully_split);
                    idents.insert(state, local_ident);
                }
            }
        }
        idents
    }

    fn construct(
        &mut self,
        og_splitting_tree: &SplittingTree,
        fsm: &Mealy,
        initial_label: &[State],
    ) {
        let all_states = initial_label.to_vec();
        let mut undistinguished = VecDeque::from([all_states]);
        let s_prime = initial_label.iter().copied();
        let curr_root_idx = self.tree.node(AdsNode::from_block(s_prime));
        self.roots.push(curr_root_idx);
        let mut unprocessed = VecDeque::from([curr_root_idx]);
        'skip_non_inj: while let Some(mut curr_node_idx) = unprocessed.pop_front() {
            let block: Vec<_> = self.tree.arena[curr_node_idx]
                .val
                .current
                .iter()
                .copied()
                .unique()
                .collect();
            let sep_seq = {
                let seq = og_splitting_tree.separating_sequence(&block);
                assert!(
                    seq.is_set(),
                    "Cannot not have a split seq if root is all states!"
                );
                if !seq.is_inj() {
                    break 'skip_non_inj;
                }
                seq.seq().clone()
            };
            let (&sep, xfer_seq) = sep_seq.split_last().expect("Sep seq cannot be empty!");
            for &input in xfer_seq {
                let r = self.mut_ref_at(curr_node_idx);
                r.input = Some(input);
                let output = r
                    .current
                    .iter()
                    .map(|&s| fsm.step_from(s, input).1)
                    .dedup()
                    .exactly_one()
                    .expect("More than one output for xfering input");
                let new_initial = r.initial.clone();
                let new_current = r
                    .current
                    .iter()
                    .map(|&s| fsm.step_from(s, input).0)
                    .collect_vec();
                if new_current.iter().unique().count() < new_initial.len() {
                    continue 'skip_non_inj;
                }
                let new_node = AdsNode::construct(new_initial, new_current);
                let new_node_idx = self.tree.node(new_node);
                let r = self.mut_ref_at(curr_node_idx);
                r.chidren.insert(output, new_node_idx);
                curr_node_idx = new_node_idx;
            }
            self.mut_ref_at(curr_node_idx).input = Some(sep);
            let r = self.mut_ref_at(curr_node_idx);
            let out_current_map: FxHashMap<_, Vec<_>> = r
                .current
                .iter()
                .zip(r.initial.iter())
                .map(|(&cur, &ini)| (fsm.step_from(cur, sep), ini))
                .fold(HashMap::default(), |mut acc, ((new_curr, out), ini)| {
                    acc.entry(out).or_default().push((new_curr, ini));
                    acc
                });
            {
                if out_current_map.values().any(|c_i_v| {
                    let (c_v, i_v): (Vec<_>, Vec<_>) = c_i_v.iter().copied().unzip();
                    c_v.iter().unique().count() < i_v.iter().unique().count()
                }) {
                    continue 'skip_non_inj;
                }
            }
            let out_node_map: FxHashMap<_, _> = out_current_map
                .into_iter()
                .map(|(out, c_i_v)| {
                    let (cv, iv): (Vec<_>, Vec<_>) = c_i_v.into_iter().unzip();
                    (out, AdsNode::construct(iv, cv))
                })
                .map(|(out, node)| (out, self.tree.node(node)))
                .collect();
            let r = self.mut_ref_at(curr_node_idx);
            r.chidren.extend(out_node_map.into_iter());
            let r = &self.tree.arena[curr_node_idx].val;
            for &c_idx in r.chidren.values() {
                let c_node = &self.tree.arena[c_idx].val;
                if c_node.current.iter().unique().count() > 1 {
                    unprocessed.push_back(c_idx);
                } else if c_node.initial.iter().unique().count() > 1 {
                    undistinguished.push_back(c_node.initial.clone());
                }
            }
        }
    }

    fn mut_ref_at(&mut self, x: usize) -> &mut AdsNode {
        &mut self.tree.arena[x].val
    }
}

#[derive(Debug)]
struct IdentStatus {
    seq: Vec<InputSymbol>,
    fully_split: bool,
}

impl IdentStatus {
    fn new(seq: &[InputSymbol], fully_split: bool) -> Self {
        Self {
            seq: seq.into(),
            fully_split,
        }
    }
    fn fully_split(&self) -> bool {
        self.fully_split
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use rstest::rstest;
    use rustc_hash::FxHashSet;

    use crate::definitions::characterisation::Characterisation;
    use crate::definitions::mealy::{InputSymbol, Mealy, State};
    use crate::definitions::FiniteStateMachine;
    use crate::util::parsers::machine::read_mealy_from_file;

    use super::HADSMethod;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    /// Ensure that the initial state is set.
    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/hypothesis_14.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    fn hads_separators(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        #[allow(clippy::needless_collect)]
        let states: Vec<_> = fsm.states().into_iter().collect();
        let hsi_idents = HADSMethod::characterisation_map(&fsm);
        for (x, y) in itertools::Itertools::tuple_combinations(states.into_iter()) {
            let x_ident = hsi_idents.get(&x).expect("Safe");
            let y_ident = hsi_idents.get(&y).expect("Safe");
            assert!(separates(&fsm, x, y, x_ident.iter()));
            assert!(separates(&fsm, x, y, y_ident.iter()));
        }
    }

    fn separates<'a>(
        fsm: &'a Mealy,
        x: State,
        y: State,
        mut seq_col: impl Iterator<Item = &'a Vec<InputSymbol>>,
    ) -> bool {
        seq_col.any(|seq| fsm.trace_from(x, seq).1 != fsm.trace_from(y, seq).1)
    }

    /// Local state identifiers should be redundancy-free.
    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/hypothesis_14.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    fn no_overlap(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        #[allow(clippy::needless_collect)]
        let states: Vec<_> = fsm.states().into_iter().collect();
        let hsi_idents = HADSMethod::characterisation_map(&fsm);

        // For each state of the fsm.
        for s in states {
            let mut other_states: FxHashSet<_> =
                fsm.states().into_iter().filter(|t| *t != s).collect();
            let local_identifiers = hsi_idents.get(&s).expect("Safe");
            for char_seq in local_identifiers {
                assert!(!other_states.is_empty());
                let s_resp = fsm.trace_from(s, char_seq).1;
                other_states.retain(|t| fsm.trace_from(*t, char_seq).1 == s_resp);
                if other_states.is_empty() {
                    break;
                }
            }
        }
    }
}
