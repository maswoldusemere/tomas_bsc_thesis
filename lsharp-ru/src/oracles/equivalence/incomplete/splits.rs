use std::collections::HashMap;

use itertools::Itertools;
// #[cfg(test)]
// mod tests;

use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(super) enum Type {
    SepInj,
    SepNonInj,
    XferInj,
    XferNonInj,
    Useless,
}

#[derive(Debug)]
pub(super) struct PartitionInfo<T> {
    split_map: FxHashMap<T, FxHashMap<State, FxHashSet<State>>>,
}

pub(super) fn analyse_input_symb(
    fsm: &Mealy,
    i: InputSymbol,
    block: &[State],
) -> PartitionInfo<OutputSymbol> {
    let mut output_dest_map: FxHashMap<_, FxHashMap<_, FxHashSet<_>>> = HashMap::default();
    block
        .iter()
        .map(|&s| (s, fsm.step_from(s, i)))
        .map(|(s, (d, o))| (s, o, d))
        .for_each(|(s, o, d)| {
            output_dest_map
                .entry(o)
                .or_default()
                .entry(d)
                .or_default()
                .insert(s);
        });
    PartitionInfo {
        split_map: output_dest_map,
    }
}

impl<T> PartitionInfo<T> {
    /// A partition is injective iff all source states which have the same output do not have the same destination state.
    fn is_injective(&self) -> bool {
        let o_d_ss_map = &self.split_map;
        // For us, this means we check if any of the values in the d_ss sub-map are not singletons.
        let mut d_ss_sub_map = o_d_ss_map.values().flat_map(HashMap::values);
        d_ss_sub_map.all(|srcs| srcs.len() < 2)
    }

    /// A partition is separating iff the set of source states produce at least two distinct outputs.
    fn is_separating(&self) -> bool {
        let o_d_ss_map = &self.split_map;
        // Simply check if the keys (which are the outputs) are more than one.
        o_d_ss_map.keys().len() > 1
    }

    pub(super) fn i_type(&self) -> Type {
        let separating = self.is_separating();
        let injective = self.is_injective();
        if separating {
            if injective {
                return Type::SepInj;
            }
            return Type::SepNonInj;
        }
        // A partition *can be* transferring if it is not separating.
        if !separating {
            if injective {
                return Type::XferInj;
            } else if !self.merges_all_states() {
                return Type::XferNonInj;
            }
        }
        // Otherwise, the partition is useless: it only has one output and one destination state.
        Type::Useless
    }

    /// A partition merges all states if there is only one destination state for all source states.
    fn merges_all_states(&self) -> bool {
        let o_d_ss_map = &self.split_map;
        let d_iter = o_d_ss_map.values().flat_map(HashMap::keys).unique();
        d_iter.count() == 1
    }

    pub(super) fn non_inj_sep_input(&self) -> bool {
        self.i_type() == Type::SepNonInj
    }

    pub(super) fn all_dests(&self) -> impl Iterator<Item = State> + '_ {
        self.split_map
            .values()
            .flat_map(HashMap::keys)
            .unique()
            .copied()
    }
}

/// Is the input word separating and injective?
pub(super) fn input_word_is_sep_inj(fsm: &Mealy, input: &[InputSymbol], r_block: &[State]) -> bool {
    let mut non_inj = FxHashSet::default();
    let mut inj = true;
    let mut num_outs = FxHashSet::default();
    for s in r_block {
        let (d, o) = fsm.trace_from(*s, input);
        inj = non_inj.insert((o.clone(), d));
        num_outs.insert(o);
    }
    let sep = num_outs.len() > 1;
    sep && inj
}
