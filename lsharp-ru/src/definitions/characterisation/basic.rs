use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;

use crate::definitions::{
    mealy::{shortest_separating_sequence, InputSymbol, Mealy, State},
    FiniteStateMachine,
};

use super::Characterisation;

/// Construct basic separating sequences for a given FSM.
///
/// This struct does not do anything special: we simply use a
/// breadth-first search to construct a characterisation set for
/// a (set of) state(s).
#[derive(Debug)]
pub struct SeparatingSeqs;

impl SeparatingSeqs {
    /// Compute an iterator for the characterisation set of `state`.
    ///
    /// The iterator contains sequences s.t. there is at least one sequence which separates
    /// `state` from another state in the FSM. The returned iterator has unique elements,
    ///  i.e., no sequence is repeated.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::SeparatingSeqs;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let s0 = State::new(0);
    /// let states: Vec<_> = fsm.states().into_iter().filter(|s| *s != s0).collect();
    /// let char_set: Vec<_> = SeparatingSeqs::char_set_state(&fsm, s0).collect();
    /// let separates = |x, seq| fsm.trace_from(x, seq) != fsm.trace_from(s0, seq);
    /// for x in states {
    ///        assert!(char_set.iter().any(|seq| separates(x, seq)));
    ///  }
    /// ```
    pub fn char_set_state(
        fsm: &Mealy,
        state: State,
    ) -> impl Iterator<Item = Vec<InputSymbol>> + '_ {
        fsm.states()
            .into_iter()
            .filter(move |s| *s != state)
            .filter_map(move |x| shortest_separating_sequence(fsm, fsm, x, state))
            .map(|(in_seq, _)| in_seq)
            .unique()
    }

    /// More general version of [`char_set_state`](Self::char_set_state),
    /// i.e., a char set for all states in `fsm`.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::SeparatingSeqs;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let states: Vec<_> = fsm.states().into_iter().collect();
    /// let char_set: Vec<_> = SeparatingSeqs::char_set(&fsm).collect();
    /// let separates = |x, y, seq| fsm.trace_from(x, seq) != fsm.trace_from(y, seq);
    /// for (x, y) in Itertools::tuple_combinations(states.into_iter()) {
    ///        assert!(char_set.iter().any(|seq| separates(x, y, seq)));
    ///  }
    /// ```
    pub fn char_set(fsm: &Mealy) -> impl Iterator<Item = Vec<InputSymbol>> + '_ {
        // tuple_combinations needs either a Vec or a Range.
        #[allow(clippy::needless_collect)]
        let states: Vec<_> = fsm.states().into_iter().collect();
        Itertools::tuple_combinations::<(State, State)>(states.into_iter())
            .filter_map(|(x, y)| shortest_separating_sequence(fsm, fsm, x, y))
            .map(|(in_seq, _)| in_seq)
            .unique()
    }
}

impl Characterisation for SeparatingSeqs {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let mut ident = BTreeMap::<_, BTreeSet<Vec<InputSymbol>>>::default();
        let pairs = fsm.states().into_iter().tuple_combinations();
        let splits =
            |seq: Vec<InputSymbol>, s, t| fsm.trace_from(s, &seq).1 != fsm.trace_from(t, &seq).1;
        for seq in Self::char_set(fsm) {
            for (s, t) in pairs.clone() {
                if splits(seq.clone(), s, t) {
                    ident.entry(s).or_default().insert(seq.clone());
                    ident.entry(t).or_default().insert(seq.clone());
                }
            }
        }
        ident
    }
}
