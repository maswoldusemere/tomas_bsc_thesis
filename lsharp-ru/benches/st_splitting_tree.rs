use std::path::Path;

use criterion::{criterion_group, criterion_main, Criterion};
use lsharp_ru::definitions::FiniteStateMachine;
use lsharp_ru::oracles::equivalence::incomplete::tree::SplittingTree;
use lsharp_ru::{definitions::mealy::Mealy, util::parsers::machine::read_mealy_from_file};

fn load_basic_fsm(name: &str) -> Mealy {
    let file_name = Path::new(name);
    read_mealy_from_file(file_name.to_str().expect("Safe")).0
}

fn mk_tree_bitvise() {
    // let fsm = load_basic_fsm("hyp_esm/hypothesis_534.dot");
    let fsm = load_basic_fsm("tests/src_models/BitVise.dot");
    let label = &fsm.states();
    let _st = SplittingTree::new(&fsm, label);
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut grp = c.benchmark_group("Splitting Tree");
    grp.sample_size(100);
    grp.bench_function("st-tree bitvise 100", |b| b.iter(mk_tree_bitvise));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
