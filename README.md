# tomas_bsc_thesis

## Building 

We depend upon the following pieces of software:
1. GCC/Clang (supporting C++20), and
2. Rust (min. version 1.67)
Please clone the repository (including subdirectories and run the following from the repository root.)
```Bash
cd gsuffix && make clean all && cd .. && cd lsharp_app && cargo b -r 
```

## Dockerfile

Alternatively, you can use the provided Dockerfile to build your own container and run the program using that.


## Running the program(s)

You can run the program as follows: from the repository's root directory, run
```Bash
# When running in the Docker container, you can use `lsharp_app` directly, without the path, as we `cargo install` the executable.
./lsharp_app/target/release/lsharp_app -m asml_rers_models/m54.dot --traces asml_rers_models/m54.traces -k 2 -s 5 --gsuffix gsuffix/gsuffix.out
```
In the command above, the arguments are:
1. m : path to the DOT file,
2. traces : path to the traces file,
3. k : number of extra states to check for,
4. s : length of the suffixes to get from the generalized suffix tree, and
5. gsuffix : path to the `gsuffix` executable.